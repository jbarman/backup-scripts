#!/bin/bash

sudo rsync -aAXv --delete --exclude={"swapfile",".Trash","lost+found",".cache",".VirtualBoxVMs",".ecryptfs"} /home/ /mnt/user-backup


######## ------ user-data daily backup cronjob start
# 0 18 * * * sudo rsync -aAXv --delete --exclude={"swapfile",".Trash","lost+found",".cache",".VirtualBoxVMs",".ecryptfs"} /home/ /mnt/user-backup
######## ------ user-data daily backup cronjob end