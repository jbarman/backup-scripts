#!/bin/bash
dir=/mnt/system-backup/backup-$(date +%Y%m%d)
mkdir -p $dir
sudo rsync -aAXv --delete  --exclude={/dev/\*,/home/\*,/proc/\*,/sys/\*,/tmp/\*,/run/\*,/mnt/\*,/media/\*,"swapfile","lost+found",".cache",".Trash","Downloads",".VirtualBoxVMs",".ecryptfs"} / $dir